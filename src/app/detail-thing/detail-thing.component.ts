import { Component, OnInit, Input } from '@angular/core';
import { Thing } from '../models/thing.model';

@Component({
  selector: 'app-detail-thing',
  templateUrl: './detail-thing.component.html',
  styleUrls: ['./detail-thing.component.css']
})
export class DetailThingComponent implements OnInit {
  @Input() thing: Thing;
  constructor() { }

  ngOnInit() {
  }

}
