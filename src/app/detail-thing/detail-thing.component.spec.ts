import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailThingComponent } from './detail-thing.component';

describe('DetailThingComponent', () => {
  let component: DetailThingComponent;
  let fixture: ComponentFixture<DetailThingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailThingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailThingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
